require 'mechanize'

class ChipdipEngine
  def initialize
    Thread.current[:threads] = []
  end

  def find(query)
    agent = Mechanize.new
    begin
      form = agent.get('https://ru-chipdip.by/').form
    rescue
      puts 'resqued in form'
      sleep 1
      retry
    end
    form.field_with(name: 'searchtext').value = query
    begin
      sleep 1
      answer = agent.submit(form)
    rescue
      puts 'resqued in submit'
      sleep 1
      retry
    end
    collect_sources(agent, answer)
    Thread.current[:threads]
  end

  private

  def collect_sources(agent, page)
    if !page.css('a[class="link group-header"]').empty?
      page.css('a[class="link group-header"]').map do |m|
        Thread.current[:threads] << Thread.new do
          begin
            p = Mechanize::Page::Link.new(m, Mechanize.new, page).click
          rescue 
            puts 'resqued in thread'
            sleep 5 * rand
            retry
          end
          Thread.current[:data] = []
          Thread.current[:data] += p.collect_data Mechanize.new
          Thread.current[:data].flatten!
        end
      end
    end
    if !page.css('a[class="link no-visited pager__control pager__next"]')[0].nil?
      link = page.css('a[class="link no-visited pager__control pager__next"]')[0]
      begin
        next_page = Mechanize::Page::Link.new(link, agent, page).click
      rescue 
        puts 'resqued in next link'
        sleep 5 * rand
        retry
      end
      collect_sources(agent, next_page)
    end
  end
end

class Mechanize::Page
  def collect_data agent
    pages = collect_pages(agent)
    pages.map do |p|
      if !p.css('div[class="item__content"]').empty?
        p.css('div[class="item__content"]').map do |m|
          d = {}
          d[:picture] = m.at_css('img').nil? ? nil : m.at_css('img')['src']
          d[:description] = m.at_css('span[class="link"]').content
          d[:price] = m.at_css('span[class="price"]').content
          d[:store] = 'www.ru-chipdip.by: ' + m.at_css('div[class="item__avail"]').at_css('div').content
          d
        end
      else
        p.css('tr[class="with-hover"]').map do |m|
          d = {}
          d[:picture] = m.at_css('img').nil? ? nil : m.at_css('img')['src']
          d[:description] = m.at_css('a[class="link"]').content
          d[:price] = m.at_css('span[class="price_mr"]').content
          d[:store] = 'www.ru-chipdip.by: ' + m.at_css('div[class="av_w2"]').content
          d
        end
      end
    end
  end

  def collect_pages agent
    pages = [self]
    if !css('a[class="link no-visited pager__control pager__next"]')[0].nil?
      link = css('a[class="link no-visited pager__control pager__next"]')[0]
      begin
        next_page = Mechanize::Page::Link.new(link, agent, self).click
      rescue
        puts 'resqued in page tab'
        sleep 5 * rand
        retry
      end
      pages += next_page.collect_pages agent
    end
    pages
  end
end