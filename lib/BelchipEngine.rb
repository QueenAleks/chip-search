require 'mechanize'

class BelchipEngine
  def self.find(query)
    agent = Mechanize.new
    begin
      form = agent.get('http://belchip.by/').form
    rescue
      puts 'resqued in belchip form'
      sleep 1
      retry
    end
    form.fields.first.value = query
    begin
      answer = agent.submit(form)
    rescue
      puts 'resqued in belchip submit'
      sleep 1
      retry
    end
    collect_data(agent, answer).compact
  end
  
  def self.collect_data agent, page
    deta = []
    page.css('div[class="cat-item"]').map do |p|
      d = {}
      d[:picture] = 'http://belchip.by/' + p.at_css('a[class="product-image"]').at_css('img')['src']
      d[:description] = p.at_css('h3').content
      d[:price] = p.css('div[class="denoPrice"]').last.content
      if p.at_css('div[class="shops"]').content.empty?
        d = nil
      else
        d[:store] = p.at_css('div[class="shops"]').content
      end
      d
    end
  end
end