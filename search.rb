require_relative 'lib/BelchipEngine.rb'
require_relative 'lib/ChipdipEngine.rb'
require 'sinatra'

class Cache
  attr_accessor :belchip_data, :chipdip_threads
end

CACHE = []

get '/' do
  erb :index
end

get '/search' do
  session = Cache.new
  CACHE << session
  @session_id = CACHE.index session
  @query = params[:query]
  session.belchip_data = BelchipEngine.find(@query)
  Thread.new { session.chipdip_threads = ChipdipEngine.new.find(@query) }
  @result = session.belchip_data.slice!(0..19)
  erb :results
end

get '/ajax' do
  @data = CACHE[params[:session_id].to_i].belchip_data.slice!(0..20)
  if @data.count < 20
    CACHE[params[:session_id].to_i].chipdip_threads.each do |t|
      while @data.count < 20 && !t[:data].empty?
        @data << t[:data].shift
      end
      break if @data.count >= 20
    end
    CACHE[params[:session_id].to_i].chipdip_threads.delete_if {|t| !t.alive? && t[:data].empty?}
  end
  @is_searching = CACHE[params[:session_id].to_i].chipdip_threads.empty? ? false : true
  erb :ajax
end